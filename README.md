# LC3 Virtual Machine

The goal of this project is it to build a virtual machine with Rust, capable of handling the LC3s instructions-set. 
The first step will be the virtual machine

## LC3

* [ISA of LC3](http://www.cs.unca.edu/~bruce/Spring14/109/Resources/lc3-isa.pdf)
* [LC3 presentation slides](https://www.cs.binghamton.edu/~tbarten1/CS120_Summer_2015/ClassNotes/L10-LC3_Intro.pdf)
* [Introduction to Computing Systems: From Bits and Gates to C and Beyond](https://highered.mheducation.com/sites/0072467509/index.html)
* [A helpful video on the ISA of the LC3](https://www.youtube.com/watch?v=4iNC2p4CN_M)
* [A really helpful video on how to write a stack virtual machine](https://github.com/pbohun/stack-vm-tutorials/tree/master/stack-vm-lessons/lesson6/sasm)


Instructions are 16 bits wide and have 4-bit opcodes

/* no opcode seen (yet) */
0x200, /* no opcode, no operands       */

	/* real instruction formats */
	0x003, /* ADD: RRR or RRI formats only */
	0x003, /* AND: RRR or RRI formats only */
	0x0C0, /* BR: I or L formats only      */
	0x020, /* JMP: R format only           */
	0x0C0, /* JSR: I or L formats only     */
	0x020, /* JSRR: R format only          */
	0x018, /* LD: RI or RL formats only    */
	0x018, /* LDI: RI or RL formats only   */
	0x002, /* LDR: RRI format only         */
	0x018, /* LEA: RI or RL formats only   */
	0x004, /* NOT: RR format only          */
	0x200, /* RTI: no operands allowed     */
	0x018, /* ST: RI or RL formats only    */
	0x018, /* STI: RI or RL formats only   */
	0x002, /* STR: RRI format only         */
	0x040, /* TRAP: I format only          */

	/* trap pseudo-op formats (no operands) */
	0x200, /* GETC: no operands allowed    */
	0x200, /* HALT: no operands allowed    */
	0x200, /* IN: no operands allowed      */
	0x200, /* OUT: no operands allowed     */
	0x200, /* PUTS: no operands allowed    */
	0x200, /* PUTSP: no operands allowed   */

	/* non-trap pseudo-op formats */
	0x0C0, /* .FILL: I or L formats only   */
	0x200, /* RET: no operands allowed     */
	0x100, /* .STRINGZ: S format only      */

	/* directive formats */
	0x040, /* .BLKW: I format only         */
	0x200, /* .END: no operands allowed    */
	0x040  /* .ORIG: I format only         */
	};

### The lc3tools

There are various files:

#### symbol.h - "interface for symbol tables of the lc3 assembler and simulator"

* define struct symbol_t
* define methods for symbol.c
#### symbol.c
*implement symbol_hash, add_symbol, find_symbol, remove_symbol_at_addr

#### lc3.def

* define the instructions (Name, mask,format,flags, code to be executed)
* define sec_cc (set condition codes macro)
* define registers and imminent values

#### lc3sim.h

* define macros for easy access to field values in instructions


