#[cfg(test)]
mod tests;

mod memory;

#[derive(Hash, Debug, Clone)]
enum InstructionType {
    ADDREGISTER,
    ADDVALUE,
    ANDREGISTER,
    ANDVALUE,
    BR,
    JMP,
    JSR,
    JSRR,
    LD,
    LDI,
    LDR,
    LEA,
    NOT,
    RET,
    RTI,
    ST,
    STI,
    STR,
    TRAP,
    RESERVED,
}

#[derive(Clone)]
struct Instruction {
    instruction_type: InstructionType,
    mask: u16,
    pattern: u16,
}

impl InstructionType {
    fn as_string(&self) -> String {
        match self {
            &InstructionType::ADDREGISTER => String::from("ADD"),
            &InstructionType::ADDVALUE => String::from("ADD"),
            &InstructionType::ANDREGISTER => String::from("AND"),
            &InstructionType::ANDVALUE => String::from("AND"),
            &InstructionType::BR => String::from("BR"),
            &InstructionType::JMP => String::from("JMP"),
            &InstructionType::JSR => String::from("JSR"),
            &InstructionType::JSRR => String::from("JSRR"),
            &InstructionType::LD => String::from("LD"),
            &InstructionType::LDI => String::from("LDI"),
            &InstructionType::LDR => String::from("LDR"),
            &InstructionType::LEA => String::from("LEA"),
            &InstructionType::NOT => String::from("NOT"),
            &InstructionType::RET => String::from("RET"),
            &InstructionType::RTI => String::from("RTI"),
            &InstructionType::ST => String::from("ST"),
            &InstructionType::STI => String::from("STI"),
            &InstructionType::STR => String::from("STR"),
            &InstructionType::TRAP => String::from("TRAP"),
            &InstructionType::RESERVED => String::from("RESERVED"),
        }
    }
}

impl PartialEq for InstructionType {
    fn eq(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }
}

impl Eq for InstructionType {}

struct Lc3 {
    instruction_masks: Vec<Instruction>,
}

impl Default for Lc3 {
    fn default() -> Self {
        let mut my_instruction_masks = Vec::with_capacity(16);
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::ADDREGISTER,
            mask: 0xF020,
            pattern: 0x1000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::ADDVALUE,
            mask: 0xF020,
            pattern: 0x1020,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::ANDREGISTER,
            mask: 0xF020,
            pattern: 0x5000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::ANDVALUE,
            mask: 0xF020,
            pattern: 0x5020,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::BR,
            mask: 0xF000,
            pattern: 0x0000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::JMP,
            mask: 0xF000,
            pattern: 0xC000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::JSR,
            mask: 0xFF00,
            pattern: 0x4800,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::JSRR,
            mask: 0xF000,
            pattern: 0x4000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::LD,
            mask: 0xF000,
            pattern: 0x2000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::LDI,
            mask: 0xF000,
            pattern: 0xA000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::LDR,
            mask: 0xF000,
            pattern: 0x6000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::LEA,
            mask: 0xF000,
            pattern: 0xE000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::NOT,
            mask: 0xF0FF,
            pattern: 0x903F,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::RET,
            mask: 0xFFF0,
            pattern: 0xC1C0,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::RTI,
            mask: 0xF000,
            pattern: 0x8000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::ST,
            mask: 0xF000,
            pattern: 0x3000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::STI,
            mask: 0xF000,
            pattern: 0xB000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::STR,
            mask: 0xF000,
            pattern: 0x7000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::TRAP,
            mask: 0xF000,
            pattern: 0xF000,
        });
        my_instruction_masks.push(Instruction {
            instruction_type: InstructionType::RESERVED,
            mask: 0xF000,
            pattern: 0xD000,
        });
        return Lc3 {
            instruction_masks: my_instruction_masks,
        };
    }
}

pub struct Lc3Vm {
    vm_specifics: Lc3,
    pc: u16,
    ir: u16,
    sp: u16,
    psr: u16,
    ssp: u16,
    usp: u16,
    condition_codes: u8,
    memory: memory::Memory<u16, u16>,
    //memory: std::collections::HashMap<u16, u16>,
    registers: [u16; 8],
}

impl Default for Lc3Vm {
    fn default() -> Self {
        let ms = 65536;
        //let ms = 100;
        Lc3Vm {
            vm_specifics: Default::default(),
            pc: 0,
            ir: 0,
            sp: 0,
            psr: 0x8,
            ssp: 0,
            usp: 0,
            condition_codes: 0x00,
            memory: memory::Memory::with_capacity(ms),
            //memory: std::collections::HashMap::with_capacity(ms),
            registers: [0; 8],
        }
    }
}

impl Lc3Vm {
    pub fn new() -> Lc3Vm {
        Default::default()
    }

    pub fn run(mut self, program: &Vec<u16>) -> Self {
        self.memory.write(0x20, 0xFFFF);
        self.memory.write(0x21, 0xFFFF);
        self.memory.write(0x22, 0xFFFF);
        self.memory.write(0x23, 0xFFFF);
        self.memory.write(0x24, 0xFFFF);
        self.memory.write(0x25, 0xFFFF);
        self.pc = program[0];
        self = self.load_in_memory(program);
        //self.pc = self.memory[0]
        while self.pc < (program[0] as usize + program.len() - 1) as u16 {
            self.ir = *Lc3Vm::load_from_memory(&self.memory, &self.pc)
                .expect("Error while fetching instruction for ir");
            self.pc += 1;
            self = self.execute_line();
        }

        return self;
    }
    fn write_to_register(registers: &mut [u16; 8], register: u16, value: u16) {
        registers[register as usize] = value;
    }
    fn execute_line(mut self) -> Self {
        for instruction_mask in self.vm_specifics.instruction_masks.iter() {
            if (self.ir & (*instruction_mask).mask) == (*instruction_mask).pattern {
                Lc3Vm::execute_instruction(
                    &mut self.condition_codes,
                    &self.memory,
                    &mut self.pc,
                    &mut self.registers,
                    instruction_mask,
                    &self.ir,
                );
            }
        }

        return self;
    }
    fn read_from_register(registers: &mut [u16; 8], register: u16) -> u16 {
        registers[register as usize]
    }
    fn load_from_memory<'a>(memory: &'a memory::Memory<u16, u16>, pc: &u16) -> Option<&'a u16> {
        memory.read(pc)
    }

    pub fn load_in_memory(mut self, program: &Vec<u16>) -> Self {
        let start_location = program[0] as usize;
        for (i, instruction) in program.iter().enumerate() {
            if i != 0 {
                self.memory
                    .write((start_location + i - 1) as u16, *instruction);
            }
        }
        let mut i = 0;
        while i < program.len() {
            if i > 0 {
                println!(
                    "The memory at address {:x} contains {:x}",
                    start_location + i - 1,
                    self.memory
                        .read(&((start_location + i - 1) as u16))
                        .expect("Fuck, this was supposed to work")
                );
            }
            i += 1;
        }

        return self;
    }

    fn set_condition_codes(condition_codes: &mut u8, value: u16) {
        // condition codes are n(egative, 0000 0001) z(ero, 0000 0010) p(ositive, 0000 0100)
        if (value & 0x8000) == 0x8000 {
            *condition_codes = 0x01;
        // value is negative
        } else {
            // value is positive
            if (value | 0x0000) == 0x0000 {
                *condition_codes = 0x02;
            } else {
                *condition_codes = 0x04;
            }
        }
    }

    fn match_condition_codes(condition_codes: &u8, binary_instruction: &u16) -> bool {
        if field_accessors::does_match(field_accessors::state_n(*binary_instruction), 0x1, 0x1) {
            if (*condition_codes & 0x01) != 0x01 {
                return false;
            }
        }
        if field_accessors::does_match(field_accessors::state_z(*binary_instruction), 0x1, 0x1) {
            if (*condition_codes & 0x02) != 0x02 {
                return false;
            }
        }
        if field_accessors::does_match(field_accessors::state_p(*binary_instruction), 0x1, 0x1) {
            if (*condition_codes & 0x04) != 0x04 {
                return false;
            }
        }
        return true;
    }

    fn execute_instruction(
        condition_codes: &mut u8,
        memory: &memory::Memory<u16, u16>,
        pc: &mut u16,
        registers: &mut [u16; 8],
        instruction: &Instruction,
        binary_instruction: &u16,
    ) {
        match instruction.instruction_type {
            InstructionType::ADDREGISTER => {
                let value = Lc3Vm::read_from_register(
                    registers,
                    field_accessors::r_sd1(*binary_instruction),
                );
                let index = field_accessors::r_rd(*binary_instruction);
                Lc3Vm::write_to_register(
                    registers,
                    index,
                    value
                        .checked_add(field_accessors::r_sd2(*binary_instruction))
                        .unwrap_or(0),
                );
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("ADD called");
            }
            InstructionType::ADDVALUE => {
                let value = Lc3Vm::read_from_register(
                    registers,
                    field_accessors::r_sd1(*binary_instruction),
                );
                let index = field_accessors::r_rd(*binary_instruction);
                Lc3Vm::write_to_register(
                    registers,
                    index,
                    value
                        .checked_add(field_accessors::r_imm5(*binary_instruction))
                        .unwrap_or(0),
                );
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("ADD called");
            }
            InstructionType::ANDREGISTER => {
                let value = Lc3Vm::read_from_register(
                    registers,
                    field_accessors::r_sd1(*binary_instruction),
                );
                let index = field_accessors::r_rd(*binary_instruction);
                Lc3Vm::write_to_register(
                    registers,
                    index,
                    value & field_accessors::r_sd2(*binary_instruction),
                );
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("AND called");
            }
            InstructionType::ANDVALUE => {
                let value = Lc3Vm::read_from_register(
                    registers,
                    field_accessors::r_sd1(*binary_instruction),
                );
                let index = field_accessors::r_rd(*binary_instruction);
                Lc3Vm::write_to_register(
                    registers,
                    index,
                    value & field_accessors::r_imm5(*binary_instruction),
                );
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("AND called");
            }
            InstructionType::BR => {
                if Lc3Vm::match_condition_codes(condition_codes, binary_instruction) {
                    *pc =
                        (*pc as i16 + (field_accessors::r_imm9(*binary_instruction) as i16)) as u16;
                    println!("BR jump called");
                } else {
                    println!("BR no jump called");
                }
            }
            InstructionType::JMP => {
                let base_r_register = field_accessors::r_sd1(*binary_instruction);
                if base_r_register == 0x7 {
                    *pc = registers[6];
                } else {
                    *pc = bin_operations::to_decimal(base_r_register, 0, 3);
                }
                println!("JMP called");
            }
            InstructionType::JSR => {
                let temp = *pc;
                if bin_operations::bit_at_position(*binary_instruction, 11) == 0x1 {
                    *pc = *pc + field_accessors::r_imm9(*binary_instruction);
                }
                registers[6] = temp;
                println!("JSR called");
            }
            InstructionType::JSRR => {
                let temp = *pc;
                if bin_operations::bit_at_position(*binary_instruction, 11) == 0x0 {
                    *pc = field_accessors::r_sd1(*binary_instruction);
                }
                registers[6] = temp;
                println!("JSRR called");
            }
            InstructionType::LD => {
                let dr = field_accessors::r_rd(*binary_instruction);
                let offset = field_accessors::r_imm9(*binary_instruction);
                let index = bin_operations::to_decimal(dr, 0, 3);
                Lc3Vm::write_to_register(registers, index, memory[*pc + offset]);
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("LD called");
            }
            InstructionType::LDI => {
                let dr = field_accessors::r_rd(*binary_instruction);
                let offset = field_accessors::r_imm9(*binary_instruction);
                let index = bin_operations::to_decimal(dr, 0, 3);
                Lc3Vm::write_to_register(registers, index, memory[memory[*pc + offset]]);
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("LDI called");
            }
            InstructionType::LDR => {
                let dr = field_accessors::r_rd(*binary_instruction);
                let base_r = field_accessors::r_sd1(*binary_instruction);
                let index = bin_operations::to_decimal(dr, 0, 3);
                let sext_offset = field_accessors::r_imm6(*binary_instruction);
                let reg_cont = Lc3Vm::read_from_register(registers, base_r);
                Lc3Vm::write_to_register(registers, index, memory[reg_cont + sext_offset]);
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("LDR called");
            }
            InstructionType::LEA => {
                let dr = field_accessors::r_rd(*binary_instruction);
                let sext_offset = field_accessors::r_imm9(*binary_instruction);
                let index = bin_operations::to_decimal(dr, 0, 3);
                Lc3Vm::write_to_register(registers, index, (*pc).overflowing_add(sext_offset).0);
                Lc3Vm::set_condition_codes(
                    condition_codes,
                    Lc3Vm::read_from_register(registers, index),
                );
                println!("LEA called");
            }
            InstructionType::NOT => {
                let dr = field_accessors::r_rd(*binary_instruction);
                let sr = field_accessors::r_sd1(*binary_instruction);
                let index = bin_operations::to_decimal(dr, 0, 3);
                let value =
                    !Lc3Vm::read_from_register(registers, bin_operations::to_decimal(sr, 0, 3));
                Lc3Vm::write_to_register(registers, index, value);
                Lc3Vm::set_condition_codes(condition_codes, value);
                println!("NOT called");
            }
            InstructionType::RET => {
                *pc = Lc3Vm::read_from_register(registers, 7);
                println!("RET called");
            }
            InstructionType::RTI => println!("RTI called"),
            InstructionType::ST => println!("ST called"),
            InstructionType::STI => println!("STI called"),
            InstructionType::STR => println!("STR called"),
            InstructionType::TRAP => {
                Lc3Vm::write_to_register(registers, 7, *pc);
                let trap_vector = field_accessors::r_imm8(*binary_instruction);
                *pc = *Lc3Vm::load_from_memory(memory, &trap_vector)
                    .expect("invalid pointer to memory");
                println!("TRAP called");
            }
            InstructionType::RESERVED => println!("a"),
        }
    }
}

mod bin_operations {
    pub fn bit_at_position(bits: u16, k: usize) -> u16 {
        if ((1 << k) & bits) > 0 {
            return 1;
        } else {
            return 0;
        }
    }
    pub fn to_decimal(bits: u16, first: u16, last: u16) -> u16 {
        let len = last - first;
        let mut result = 0;
        for i in 0..len {
            let x = bit_at_position(bits, (i + first) as usize);
            result += power(2, i as usize) * x;
        }
        return result;
    }
    pub fn power(number: u16, exponent: usize) -> u16 {
        if exponent < 1 {
            return 1;
        } else if exponent == 1 {
            return number;
        } else {
            return number * power(number, exponent - 1);
        }
    }
    pub fn sextend(number: u16) -> u16 {
        if number == 0 {
            return number;
        } else {
            return number | ones(number.leading_zeros());
        }
    }
    fn ones(amount: u32) -> u16 {
        let mut sextended: u16 = 0x0000;
        let mut x = 0;
        while x < amount {
            //    sextended |= 1 << (15 - x);
            sextended |= (1 as u16).overflowing_shl((15 - x) as u32).0;
            x += 1;
        }
        return sextended;
    }
}

mod field_accessors {
    pub fn does_match(a: u16, mask: u16, pattern: u16) -> bool {
        return (a & mask) == pattern;
    }
    pub fn state_n(instruction: u16) -> u16 {
        return (instruction >> 11) & 0x1;
    }
    pub fn state_z(instruction: u16) -> u16 {
        return (instruction >> 10) & 0x1;
    }
    pub fn state_p(instruction: u16) -> u16 {
        return (instruction >> 9) & 0x1;
    }
    pub fn r_rd(instruction: u16) -> u16 {
        return (instruction >> 9) & 0x7;
    }
    pub fn r_sd1(instruction: u16) -> u16 {
        return (instruction >> 6) & 0x7;
    }
    pub fn r_sd2(instruction: u16) -> u16 {
        return instruction & 0x7;
    }
    pub fn r_imm5(instruction: u16) -> u16 {
        let mut result: u16;
        if instruction & 0x0010 == 0 {
            result = instruction & 0x000F;
        } else {
            result = instruction | !0x000F;
        }
        if (result & (1 << 4)) == 1 {
            result = super::bin_operations::sextend(result);
        }
        return result;
    }
    pub fn r_imm6(instruction: u16) -> u16 {
        let mut result: u16;
        if instruction & 0x0020 == 0 {
            result = instruction & 0x001F;
        } else {
            result = instruction | !0x001F;
        }
        if (result & (1 << 5)) == 1 {
            result = super::bin_operations::sextend(result);
        }
        return result;
    }
    pub fn r_imm8(instruction: u16) -> u16 {
        let mut result: u16;
        if instruction & 0x0080 == 0 {
            result = instruction & 0x007F;
        } else {
            result = instruction | !0x007F;
        }
        if (result & (1 << 7)) == 1 {
            result = super::bin_operations::sextend(result);
        }
        return result;
    }
    pub fn r_imm9(instruction: u16) -> u16 {
        let mut result: u16;
        if instruction & 0x100 == 0 {
            result = instruction & 0x01FF;
        } else {
            result = instruction | !0x01FF;
        }
        if (result & (1 << 8)) == 1 {
            result = super::bin_operations::sextend(result);
        }
        return result;
    }
    pub fn r_imm11(instruction: u16) -> u16 {
        let mut result: u16;
        if instruction & 0x400 == 0 {
            result = instruction & 0x03FF;
        } else {
            result = instruction | !0x03FF;
        }
        if (result & (1 << 7)) == 1 {
            result = super::bin_operations::sextend(result);
        }
        return result;
    }
}
