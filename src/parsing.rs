enum State {
    START,
    SKIP,
    READINSTRUCTION,
}

#[derive(Default)]
struct Lexer {
    beg_char: char,
    end_char: char,
}

impl Lexer {
    fn get_instruction(s: &String) -> Option<InstructionType> {
        if (s.starts_with(&InstructionType::ADDREGISTER.as_string())) {
            Some(InstructionType::ADDREGISTER)
        } else {
            None
        }
    }
    fn is_some<InstructionType>(optional_value: Option<InstructionType>) -> bool {
        match optional_value {
            Some(x) => true,
            None => false,
        }
    }
    fn is_comment(s: String) -> bool {
        s.starts_with(";;")
    }
    fn parse_line(line: &String) -> String {
        //line.starts_with(InstructionType::ADDREGISTER.as_string());
        return String::new();
    }
    pub fn lex(code: Vec<String>) -> Vec<String> {
        let mut result_vec = Vec::with_capacity(code.len());
        let mut state = State::START;
        let mut line = String::new();
        for (pos, e) in code.iter().enumerate() {
            match state {
                State::START => {
                    line = String::from(e.trim());
                    if (Lexer::is_some(Lexer::get_instruction(&line))) {
                        state = State::READINSTRUCTION;
                    } else if (Lexer::is_comment(line)) {
                        state = State::SKIP;
                    }
                }
                State::READINSTRUCTION => result_vec.push(Lexer::parse_line(&line)),
                State::SKIP => state = State::START,
            }
            result_vec.push(Lexer::parse_line(&String::from(e)));
            println!("{}", e);
        }
        return result_vec;
    }
}
