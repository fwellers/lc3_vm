use crate::bin_operations;
use crate::Lc3Vm;

#[test]
fn load_program_in_memory() {
    let mut test_vm: Lc3Vm = Default::default();
    let mut program = Vec::new();
    program.push(0x3000);
    program.push(0x54A0); // this is 0101 0100 1010 0000
    program.push(0x56E0);
    program.push(0x14A2);
    program.push(0x16A3);
    program.push(0xF025);
    test_vm = test_vm.load_in_memory(&program);

    assert_eq!(test_vm.memory[0x3000], 0x54A0);
    assert_eq!(test_vm.memory[0x3001], 0x56E0);
    assert_eq!(test_vm.memory[0x3002], 0x14A2);
    assert_eq!(test_vm.memory[0x3003], 0x16A3);
    assert_eq!(test_vm.memory[0x3004], 0xF025);
}

#[test]
fn run_simple_program() {
    let mut test_vm: Lc3Vm = Default::default();
    let mut program = Vec::new();
    program.push(0x3000);
    program.push(0x54A0); // this is 0101 0100 1010 0000
    program.push(0x56E0);
    program.push(0x14A2);
    program.push(0x16A3);
    program.push(0xF025);
    test_vm = test_vm.run(&program);
    // test for decimal representation since we dont get hex back
    assert_eq!(test_vm.memory[0x3000], 0x54A0);
    assert_eq!(Lc3Vm::read_from_register(&mut test_vm.registers, 2), 2);
    assert_eq!(Lc3Vm::read_from_register(&mut test_vm.registers, 3), 5);
}
#[test]
fn test_binary_to_decimal() {
    let test = 0xF200; // this corresponds to 1111 0010 0000 0000
    let result = bin_operations::to_decimal(test, 12, 16);
    assert_eq!(result, 15);
}

#[test]
fn test_sextend() {
    let number: u16 = 0x0500;
    let result = bin_operations::sextend(number);
    assert_eq!(result, 0xFD00);
}

#[test]
fn test_second_programm() {
    let mut test_vm: Lc3Vm = Default::default();
    let mut program = Vec::new();
    program.push(0x3000); // 0011 0000 0000 0000 || start pc at 0x3000
    program.push(0x5260); // 0101 0010 0110 0000 || AND R1,R1 0
    program.push(0x5920); // 0101 1001 0010 0000 || AND R4,R4 0
    program.push(0x192A); // 0001 1001 0010 1010 || ADD R4,R4 xA
    program.push(0xE4FC); // 1110 0100 1111 1100 || LEA R2 x0FC --> R2 = PC + x0FC = 3100
    program.push(0x6680); // 0110 0110 1000 0000 || LDR R3,R2 x0 --> R3 = mem[3260]
    program.push(0x14A1); // 0001 0100 1010 0001 || ADD R2,R2 x1
    program.push(0x1243); // 0001 0010 0100 0011 || ADD R1,R1 R3
    program.push(0x193F); // 0001 1001 0011 1111 || ADD R4,R4 x1F
    program.push(0x03FB); // 0000 0011 1111 1011 || BR LOOP x1FB
    program.push(0xF025); // 1111 0000 0010 0101 || HALT

    let mut data = Vec::new();
    data.push(0x3100);
    data.push(0x3107); // 0
    data.push(0x2819); // 1
    data.push(0x0110); // 2
    data.push(0x0310); // 3
    data.push(0x0110); // 4
    data.push(0x1110); // 5
    data.push(0x11B1); // 6
    data.push(0x0019); // 7
    data.push(0x0007); // 8
    data.push(0x0004); // 9

    test_vm = test_vm.load_in_memory(&data);
    test_vm = test_vm.run(&program);
}
