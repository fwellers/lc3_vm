#[derive(Default, Clone, Debug)]
pub struct Memory<U, T>
where
    U: std::hash::Hash,
    U: std::cmp::Eq,
{
    size: usize,
    contents: std::collections::HashMap<U, T>,
}

impl<U, T> Memory<U, T>
where
    U: std::hash::Hash,
    U: std::cmp::Eq,
{
    pub fn read(&self, address: &U) -> Option<&T> {
        self.contents.get(address)
    }
    pub fn read_mut(&mut self, address: &U) -> Option<&mut T> {
        self.contents.get_mut(address)
    }
    pub fn write(&mut self, address: U, value: T) {
        if self.size > self.contents.len() {
            self.contents.insert(address, value);
        }
    }
    pub fn with_capacity(capacity: usize) -> Memory<U, T> {
        Memory {
            size: capacity,
            contents: std::collections::HashMap::with_capacity(capacity),
        }
    }
}

impl<U, T> std::ops::Index<U> for Memory<U, T>
where
    U: std::hash::Hash,
    U: std::cmp::Eq,
{
    type Output = T;
    fn index(&self, index: U) -> &Self::Output {
        &self.contents[&index]
    }
}

impl<U, T> std::ops::IndexMut<U> for Memory<U, T>
where
    U: std::hash::Hash,
    U: std::cmp::Eq,
{
    fn index_mut(&mut self, index: U) -> &mut Self::Output {
        self.contents
            .get_mut(&index)
            .expect("no entry found at index")
    }
}
